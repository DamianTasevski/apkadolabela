package apkadolabela2.apkadolabela2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
public class cont {
    private static final Logger logger = LogManager.getLogger(cont.class);

    @GetMapping
    String getHello() {
        int a = 0;
        //RevDeBug.Storage.getStorageApi().StoreSnapshot("HOaGf4cMFd186JsyP5ZMK43RHOxtzKWUvhmj7Ftfb0nf7AQNbnGL2Yh4ezoZ8YOOdnmX2mLGbCwyOglZC0c6jNT5bwEVfqBNoKM8gkOmoJWsvwWQOhXosQYkuULZ549hDWOjwEkio0xw6fVpyHEGZSWhP2oCW0LrtD8ydkEgXBdUpl9ItDZjcw8j105IbKxH5lq0izh0HQQ7bIT4k1d0oQmEVBUy9KXbI9TKHNSttMchNvjlz1B4gkFaCKuzIBZxTcsMqHgPUHP5KXCBeq8swqntK26G3DejM1Jx65GAcLFmc41SI54YfPhaksgNuOJ7RxqAJ7SttY3Yytzc6Or63WbB7q6nNfKdJk5kWjTAaZzv3dfCHgtAWCVyZlOpcWyavvt50DjNw8ObjMs3");
        RevDeBug.Storage.getStorageApi().StoreSnapshot("system_out_print");
        return "hello";
    }

    @GetMapping("/err")
    String getErrror() {
        int multiplier = 12;
        int val = 0;
        for (int i = 10; i >= 0;) {
            val = val + multiplier / i;
            i = i - 1;
        }

        RevDeBug.Storage.getStorageApi().StoreSnapshot("system_out_print");
        return String.valueOf(val);
    }

    @GetMapping("/logerror")
    String logError() {
        int multiplier = 12;
        int val = 0;

        try {
            for (int i = 10; i >= 0; ) {
                val = val + multiplier / i;
                i = i - 1;
            }
        }catch (Exception e){
            logger.error(e);
        }

        //RevDeBug.Storage.getStorageApi().StoreSnapshot("system_out_print");
        return String.valueOf(val);
    }

    @GetMapping("/test")
    String test() {

        String test = "test";

        for (int i = 0; i < 10; i++ ) {
            test = test + "_" + test;
        }

        return String.valueOf(test);
    }
}
