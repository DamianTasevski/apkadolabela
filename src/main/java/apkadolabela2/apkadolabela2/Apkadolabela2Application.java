package apkadolabela2.apkadolabela2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apkadolabela2Application {

    public static void main(String[] args) {
        SpringApplication.run(Apkadolabela2Application.class, args);
    }

}
